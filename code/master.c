#include <stdio.h>
#include <stdlib.h>

#include <woden-heart.h>



int main(int argc, char **argv)
{
    enum e_woden_faults v_fault = ef_success;
    struct s_woden v_woden = f_initialise_woden();



    struct s_woden_ciphed_command
    v_first_command = f_ciphe_woden_command(
        ec_assingment,
        3, __common_collection1(t_unsigned8, 3)
        {
            1, 0, 12
        }
    ),
    v_second_command = f_ciphe_woden_command(
        ec_assingment,
        3, __common_collection1(t_unsigned8, 3)
        {
            3, 0, 4
        }
    ),
    v_third_command = f_ciphe_woden_command(
        ec_multiplication,
        4, __common_collection1(t_unsigned8, 4)
        {
            1, 0, 3, 0
        }
    ),
    v_fourth_command = f_ciphe_woden_command(
        ec_stop,
        0, __common_null
    );



    v_woden = f_add_ciphed_woden_command(v_woden, v_first_command);
    v_woden = f_add_ciphed_woden_command(v_woden, v_second_command);
    v_woden = f_add_ciphed_woden_command(v_woden, v_third_command);
    v_woden = f_add_ciphed_woden_command(v_woden, v_fourth_command);

    v_woden = f_handle_woden(v_woden, &v_fault);



    printf("fault result -> %u\n", v_fault);
    printf("result -> %u\n", v_woden.v_cells_unsigned8[1].v_frame);

    getchar();

    return 0;
}
