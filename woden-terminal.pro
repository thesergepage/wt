TEMPLATE = app
TARGET = woden-terminal.elf
CONFIG = console ordered warn_on exceptions_off static



# development-stage
QMAKE_CFLAGS = -pedantic -W -Wall -Wextra -Wpedantic -std=gnu89 -funsigned-bitfields -funsigned-char -fsso-struct=little-endian -pipe -g3 -fvar-tracking -gdescribe-dies -ggnu-pubnames -grecord-gcc-switches -gas-loc-support -gas-locview-support -gcolumn-info -gstatement-frontiers -gdwarf -std=gnu89
QMAKE_LIBDIR_FLAGS = -L../woden-heart/



SOURCES += \
    code/master.c

INCLUDEPATH += ../woden-heart/code
DEPENDPATH += ../woden-heart/code

LIBS += -lwoden-heart
